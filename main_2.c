#include <stdio.h>
#include <stdlib.h>
void fibonacciSeq(int n){
    static int num1=0,num2=1,sum;
    if(n>0){
         sum = num1 + num2;
         num1 = num2;
         num2 = sum;
         printf("%d\n",sum);
         fibonacciSeq(n-1);
    }
}
int main(){
    int n;
    printf("Enter any integer\n");
    scanf("%d",&n);
    printf("%d\n%d\n",0,1);
   fibonacciSeq(n-2);
  return 0;
 }
